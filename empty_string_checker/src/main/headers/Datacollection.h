#ifndef UNTITLEDCLASSES_COLLECTION_H
#define UNTITLEDCLASSES_COLLECTION_H

#include <iostream>
#include <list>
#include <string>
#include <utility>

#include "Contract.h"
#include "Service.h"
#include "Group.h"
#include "Worker.h"
#include "Job.h"

class Collection {
protected:
  Collection(std::list<Contract> Contracts, std::list<Service> Services,
             std::list<Group> Groups, std::list<Worker> Workers,
             std::list<Job> Jobs);
  static Collection* collection_;
public:
  std::list<Contract> contracts;
  std::list<Service> services;
  std::list<Group> groups;
  std::list<Worker> workers;
  std::list<Job> jobs;

  Collection(Collection &other) = delete
  static Collection *GetInstance(std::list<Contract> Contracts, std::list<Service> Services,
             std::list<Group> Groups, std::list<Worker> Workers,
             std::list<Job> Jobs);
};

#endif // UNTITLEDCLASSES_COLLECTION_H