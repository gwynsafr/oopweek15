#ifndef UNTITLEDCLASSES_APPLICATION_H
#define UNTITLEDCLASSES_APPLICATION_H

#include <iostream>

class Application {
public:
  Application();

  void run();
};

#endif // UNTITLEDCLASSES_APPLICATION_H