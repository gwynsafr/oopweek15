#ifndef UNTITLEDCLASSES_SIMPLEREPORT_H
#define UNTITLEDCLASSES_SIMPLEREPORT_H

#include <iostream>
#include <string>
#include <utility>

class SimpleReport {
public:
    template<class T> void print() = 0;
};

#endif // UNTITLEDCLASSES_SIMPLEREPORT_H