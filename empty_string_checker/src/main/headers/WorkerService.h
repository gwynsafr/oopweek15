#ifndef UNTITLEDCLASSES_WorkerService_H
#define UNTITLEDCLASSES_WorkerService_H

#include <iostream>
#include <string>
#include <utility>
#include <list>

#include "Datacollection.h"

class WorkerService {
public:
  std::list<Worker> searchByJob(std::list<Worker>);
};

#endif // UNTITLEDCLASSES_WorkerService_H