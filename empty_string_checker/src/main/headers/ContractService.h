#ifndef UNTITLEDCLASSES_ContractService_H
#define UNTITLEDCLASSES_ContractService_H

#include <iostream>
#include <string>
#include <utility>
#include <list>

#include "Datacollection.h"

class ContractService {
public:
  std::list<Contract> getActiveContracts(std::list<Contract>);
  std::list<Contract> countServicesUsage(std::list<Contract> contracts)
};

#endif // UNTITLEDCLASSES_ContractService_H