#include "Group.h"
#include "Worker.h"

Group::Group() = default;
Group::Group(long id, std::list<Worker> workers) {
  this->id = id;
  this->workers = workers;
};
