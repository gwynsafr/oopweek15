#include "Event.h"
#include "Group.h"

Event::Event() = default;
Event::Event(long id, std::string date, Group group, std::string description) {
  this->id = id;
  this->date = date;
  this->group = group;
  this->description = description;
};