#include "Contract.h"

Contract::Contract() = default;
Contract::Contract(long id, Client client, std::list<ProvidedService> services,
                   std::string contract_start_date,
                   std::string contract_end_date, std::list<Group> groups,
                   std::string status) {
  this->client = client;
  this->services = services;
  this->contract_start_date = contract_start_date;
  this->contract_end_date = contract_end_date;
  this->groups = groups;
  this->status = status;
};