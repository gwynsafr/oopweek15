#include "Hourlyjob.h"

Hourlyjob::Hourlyjob() = default;
Hourlyjob::Hourlyjob(long id, std::string name, double PayRate,
                     int HoursWorked) {
  Job::setId(id);
  this->name = name;
  this->PayRate = PayRate;
  this->HoursWorked = HoursWorked;
};

double Hourlyjob::calculatePayment() {
  return this->PayRate * this->HoursWorked;
};