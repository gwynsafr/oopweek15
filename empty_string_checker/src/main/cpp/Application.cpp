#include "Application.h"
#include "Datacollection.cpp"
#include "Mainscreen.cpp"

Application::Application() = default;

void Application::run() {
  Mainscreen first_page;
  first_page.welcoming_message();
  first_page.available_options();
};