#include "SimpleReport.h"

template <class T> void SimpleReport::print(std::list<T> onetypetables) {
    for (T table : onetypetables) {
        std::cout << table << std::endl;
    }
}