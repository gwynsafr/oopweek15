#include "ContractService.h"
#include <map>

std::list<Contract> ContractService::getActiveContracts(std::list<Contract> contracts) {
  std::list<Contract> ActiveContracts;
  for (Contract contract : contracts) {
    if (contract.getStatus() == "Active") {
      ActiveContracts.push_back(contract);
    }
  }
  return ActiveContracts;
}

std::list<Contract> ContractService::countServicesUsage(std::list<Contract> contracts) {
  std::map<ProvidedService, int> popularservice;
  for (Contract contract : contracts) {
    for (ProvidedService service : contract.getServices()) {
      if (popularservice.count(service) >= 1) {
        popularservice[service] = popularservice[service] + 1;
      }
      else {
        popularservice[service] = 1;
      }
    }
  }
}