#include "Datacollection.h"

Collection::Collection(std::list<Contract> Contracts, std::list<ProvidedService> Services,
                       std::list<Group> Groups, std::list<Worker> Workers,
                       std::list<Job> Jobs) {
    this->contracts = Contracts;
    this->services = Services;
    this->groups = Groups;
    this->workers = Workers;
    this->jobs = Jobs;
    };

Collection *Collection::GetInstance(std::list<Contract> Contracts, std::list<Service> Services,
             std::list<Group> Groups, std::list<Worker> Workers,
             std::list<Job> Jobs) {
    if(collection_==nullptr) {
        collection_ = new Collection(std::list<Contract> Contracts, std::list<Service> Services,
             std::list<Group> Groups, std::list<Worker> Workers,
             std::list<Job> Jobs);
    }
    return collection_;
}