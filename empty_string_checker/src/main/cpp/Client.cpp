#include "Client.h"
#include "Event.h"

Client::Client() = default;
Client::Client(long id, std::string name, std::string requisites,
               std::string address, std::string phone, std::list<Event> events) {
    this->id = id;
    this->name = name;
    this->requisites = requisites;
    this->address = address;
    this->phone = phone;
    this->events = events;
    };